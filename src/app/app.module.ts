import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NgtableComponent } from './ngtable/ngtable.component';
import {ApiService} from "./services/apiServices";

@NgModule({
  declarations: [
    AppComponent,
    NgtableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
      ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
