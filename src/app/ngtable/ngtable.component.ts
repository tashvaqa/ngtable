import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ApiService} from "../services/apiServices";

@Component({
  selector: 'app-ngtable',
  templateUrl: './ngtable.component.html',
  styleUrls: ['./ngtable.component.css']
})
export class NgtableComponent implements OnInit {
    @Input() testTitle: string;
    @Output() clickedMe = new EventEmitter<any>();
  tableData: any;
  arrayOfKeys: any;
  error: any;

  constructor(
      private http: HttpClient,
      private apiService: ApiService
  ) { }

  ngOnInit() {
/*      console.log(this.testTitle);
      this.apiService.getData.then()
    this.http.get('http://localhost:4200/assets/sample_data.json')
        .subscribe(data => {
          this.tableData = data;
          this.arrayOfKeys = Object.keys(data[0]);
        }, error => {
            this.error = error.statusText;
        }, () => {
            //do some action here
        });*/
        this.getTableData();
  }

  getTableData(): void {
      this.apiService.getData().subscribe((data) => {
          this.tableData = data;
          this.arrayOfKeys = Object.keys(data[0]);
      });
  }

  handleClick(): void {
      this.clickedMe.emit('someone clicked me');
  }

}
