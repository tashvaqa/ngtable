import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {catchError, tap} from "rxjs/internal/operators";
@Injectable()
export class ApiService {
    constructor(private http: HttpClient){}
    getData(): Observable<any> {
        return this.http.get('http://localhost:4200/assets/sample_data.json').pipe(
            tap(data => {
                console.log('feched the table data');
            })
        );
    }
}